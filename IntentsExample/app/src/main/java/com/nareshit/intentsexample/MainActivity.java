package com.nareshit.intentsexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Handling the Button Click
    public void takeMeNext(View view)
    {
        // Logic to open the SecondActivity.class has to be written here
        // Completed: Create an Intent Object (explicit) with source and destination.
        Intent i = new Intent(this,SecondActivity.class);
        // Completed: start the Activity with the help of startActivity()
        startActivity(i);
    }
}