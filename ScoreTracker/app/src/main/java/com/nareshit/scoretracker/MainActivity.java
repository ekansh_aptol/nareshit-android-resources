package com.nareshit.scoretracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mResult;
    private Button plus_btn, minus_btn;
    private int mCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mResult = findViewById(R.id.result_tv);
        plus_btn = findViewById(R.id.plusBtn);
        minus_btn = findViewById(R.id.minusBtn);

        plus_btn.setOnClickListener(this);
        minus_btn.setOnClickListener(this);

        /*plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle Plus Btn Logic
                mCount++;
                mResult.setText(String.valueOf(mCount));
            }
        });

        minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle Minus Btn logic
                mCount--;
                mResult.setText(String.valueOf(mCount));
            }
        });*/
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plusBtn){
            mCount++;
        }else{
            mCount--;
        }

        mResult.setText(String.valueOf(mCount));
    }
    public void scoreIncrease(View view) {
        // when the '+' button is clicked, This method gets called.
        // we can handle the button click (logically) from this method
        // Toast.makeText(this, "Button + is Clicked", Toast.LENGTH_SHORT).show();
        // Completed: First Increment the value
        mCount++;
        // Completed: Show up the Content On the TextView
        mResult.setText(String.valueOf(mCount));
        // Completed : get the current value of the textview in string format
        // String value = mResult.getText().toString();
        // Completed : convert the string value to integer value
        // int v = Integer.parseInt(value);
        // completed : Increment the value
        // v++;
        // Completed : display the value directly on top of the textview
        // mResult.setText(String.valueOf(v));
    }


    public void scoreDecrease(View view) {
        // when the '-' button is clicked, This method gets called.
        // we can handle the button click (logically) from this method
        // Toast.makeText(this, "Button - is clicked", Toast.LENGTH_SHORT).show();
        // completed : First Decrement the value
        mCount--;
        // TODO 4: Show up the Content On the TextView
        mResult.setText(String.valueOf(mCount));
    }


}