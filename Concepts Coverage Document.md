# 18-10-2021
## Agenda - Concepts Covered on the day!
#### Layouts, Views and Viewgroups
> *View - What ever the UI Components you see on an Android App is called a VIEW. EX: Buttons, EditText, Slider, RadioButton, etc.,
> *ViewGroup - A ViewGroup is a View that can accommodate other Views and ViewGroups inside it. 
> *Layouts - Layouts gives us the structure to organize the Views of our Screen - all Layouts are ViewGroups. The Root view is always a ViewGroup.
#### [Layout Rules - Click Here](https://bitbucket.org/pavankumart46/nareshit-android-resources/raw/eee1e2aea74f0064ed66f4b51b6b360a101fc6cb/ScoreTracker/app/src/main/res/values/XMLRules.xml/)
#### [Changing the existing layout to Linear Layout of Vertical Orientation - Click Here](ScoreTracker/app/src/main/res/layout/activity_main.xml)

# 19-10-2021
## Agenda - Concepts covered on the day!
#### Desiging the user interface
1. We use SP as the units for changing the textSize - SP - Scalable Pixels.
2. We use DP as the units for setting the dimensions for thee Views - DP- Display Pixels.
[Layout Design Code](ScoreTracker/app/src/main/res/layout/activity_main.xml)


[Handling Button Clicks - Code](ScoreTracker/app/src/main/java/com/nareshit/scoretracker/MainActivity.java)

# 20-10-2021
## Agenda - Concepts Covered on the day!
#### Connecting the textview to the java file (object reference - findviewbyId() method)
#### How to set the text on the TextView using settext() method ?
#### How to Read the documentation related to [TextView](https://developer.android.com/reference/android/widget/TextView)
#### How to Check error Log using logCat?
#### Alternatives to Button's onClick attribute. (SetOnClickListner() method and using OnClickListener() Interface)
[activity_main.xml](ScoreTracker/app/src/main/res/layout/activity_main.xml)
[MainActivity.java](ScoreTracker/app/src/main/java/com/nareshit/scoretracker/MainActivity.java)


# 21-10-2021
## Agenda - Concepts Covered on the day!
#### What are activities?
#### How to create an activity?
#### Intents
#### Types of Intents
	-	 Explicit Intents
	- 	 Implicit Intents
#### Worked with Explicit Intents Example [Click Here for the example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/IntentsExample/)
#### [For Presentation that is used to explain intents and activities Click Here](https://docs.google.com/presentation/d/1kjxsI9brdVRIx3rqoB0H-1-PmVlzJbiQNf4PyqzZKJM/edit#slide=id.g181baec304_0_0)